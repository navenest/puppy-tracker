package pg

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	logz "gitlab.com/navenest/puppy-tracker/pkg/logger/log"
	"go.uber.org/zap"
	"os"
	"strconv"
	"time"
)

var DB *sql.DB

func initPGDB() *sql.DB {
	var host string
	if value, ok := os.LookupEnv("DB_HOST"); ok {
		host = value
	} else {
		host = "postgres"
	}
	var port int
	if value, ok := os.LookupEnv("DB_PORT"); ok {
		port, _ = strconv.Atoi(value)
	} else {
		port = 5432
	}
	var user string
	if value, ok := os.LookupEnv("DB_USER"); ok {
		user = value
	} else {
		user = "skippy"
	}
	var password string
	if value, ok := os.LookupEnv("DB_PASS"); ok {
		password = value
	} else {
		password = "skippy123"
	}
	var dbname string
	if value, ok := os.LookupEnv("DB_NAME"); ok {
		dbname = value
	} else {
		dbname = "puppy_tracker"
	}

	psqlconn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	db, err := sql.Open("postgres", psqlconn)
	if err != nil {
		logz.Logger.Error("Unable to connect to DB",
			zap.Error(err),
			zap.String("host", host))
	}

	err = db.Ping()
	if err != nil {
		logz.Logger.Error("Unable to ping DB",
			zap.Error(err),
			zap.String("host", host))
	}

	// Create public schema
	res, err := db.Exec(`CREATE SCHEMA IF NOT EXISTS public`)
	if err != nil {
		logz.Logger.Error("Unable to create Schema",
			zap.Error(err))
	}
	rows, err := res.RowsAffected()
	if err != nil {
		logz.Logger.Error("Unable to display Rows",
			zap.Error(err))
	}
	logz.Logger.Info("Rows from schema create",
		zap.Int64("rows", rows))

	// Create events Table
	res, err = db.Exec(`CREATE TABLE IF NOT EXISTS events (
    	user_name TEXT NOT NULL,
    	animal_name TEXT NOT NULL,
    	event TEXT NOT NULL,
    	event_timestamp TIMESTAMP NOT NULL
        )`)
	if err != nil {
		logz.Logger.Error("Unable to create Tables",
			zap.Error(err))
	}
	rows, err = res.RowsAffected()
	if err != nil {
		logz.Logger.Error("Unable to display Rows",
			zap.Error(err))
	}
	logz.Logger.Info("Rows from table create",
		zap.Int64("rows", rows))

	db.SetMaxOpenConns(25)
	db.SetMaxIdleConns(25)
	db.SetConnMaxLifetime(5 * time.Minute)

	return db
}

func init() {
	logz.Logger.Info("Connecting to DB")
	DB = initPGDB()
	logz.Logger.Info("Connected to DB")
}
