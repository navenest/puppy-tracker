package pg

import (
	"context"
	"fmt"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
	"testing"
)

type pgContainer struct {
	testcontainers.Container
	URI string
}

func setupPG(ctx context.Context) (*pgContainer, error) {
	req := testcontainers.ContainerRequest{
		Image:        "postgres",
		ExposedPorts: []string{"5432/tcp"},
		Env: map[string]string{
			"POSTGRES_USER":     "skippy",
			"POSTGRES_PASSWORD": "skippy123",
			"POSTGRES_DB":       "scavenger_hunt",
		},
		WaitingFor: wait.ForLog("* database system is ready to accept connections"),
		//"database system is ready to accept connections"
	}
	container, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})
	if err != nil {
		return nil, err
	}

	mappedPort, err := container.MappedPort(ctx, "5432")
	if err != nil {
		return nil, err
	}

	hostIP, err := container.Host(ctx)
	if err != nil {
		return nil, err
	}

	uri := fmt.Sprintf("%s:%s", hostIP, mappedPort.Port())

	return &pgContainer{Container: container, URI: uri}, nil
}

func TestInitPGDB(t *testing.T) {
	//ctx := context.Background()
	//pgContainer, err := setupPG(ctx)
	//if err != nil {
	//	t.Fatal(err)
	//}
	//defer pgContainer.Terminate(ctx)

}
