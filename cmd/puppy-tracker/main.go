package main

import (
	"context"
	"errors"
	"gitlab.com/navenest/golang-library/pkg/opentelemetry"
	"gitlab.com/navenest/puppy-tracker/pkg/database/pg"
	logz "gitlab.com/navenest/puppy-tracker/pkg/logger/log"
	"gitlab.com/navenest/puppy-tracker/web"
	"go.uber.org/zap"
	"os"
	"os/signal"
)

func main() {
	logz.Logger.Info("logger construction succeeded")
	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt)
	defer stop()

	// Set up OpenTelemetry.
	otelShutdown, err := opentelemetry.SetupOTelSDK(ctx)
	if err != nil {
		return
	}
	// Handle shutdown properly so nothing leaks.
	defer func() {
		err = errors.Join(err, otelShutdown(context.Background()))
	}()
	err = pg.DB.Ping()
	if err != nil {
		logz.Logger.Error("Unable to ping DB",
			zap.Error(err))
	}
	webserver := web.App{}
	webserver.Initialize()
	webserver.Run(":8080")
	err = pg.DB.Close()
	if err != nil {
		logz.Logger.Error("Unable to Close DB Connection",
			zap.Error(err))
	}
}
