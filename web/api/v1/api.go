package v1

import (
	"database/sql"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/navenest/puppy-tracker/pkg/database/pg"
	logz "gitlab.com/navenest/puppy-tracker/pkg/logger/log"
	"go.uber.org/zap"
	"net/http"
	"time"
)

type health struct {
	Status string `json:"status"`
}

type API struct {
	Status string `json:"status"`
	Router *gin.Engine
}

type Event struct {
	Username   string    `json:"user_name"`
	Timestamp  time.Time `json:"timestamp"`
	AnimalName string    `json:"animal_name"`
	Event      string    `json:"event"`
}

type Events struct {
	Events []Event `json:"events"`
}

// Probably a better way to do this
type GetEvent struct {
	Username   string `json:"user_name"`
	Timestamp  string `json:"timestamp"`
	AnimalName string `json:"animal_name"`
	Event      string `json:"event"`
}

type GetEvents struct {
	Events []GetEvent `json:"events"`
}

type Middleware func(http.HandlerFunc) http.HandlerFunc

//TODO Make this better, move this outside of just the global vars?

func (api *API) getEvents(c *gin.Context) {
	rows, err := pg.DB.Query(`SELECT user_name,animal_name,event,event_timestamp FROM events ORDER BY event_timestamp desc limit 50`)
	if err != nil {
		logz.Logger.Error("Unable to query for events",
			zap.Error(err))
	}
	defer func(rows *sql.Rows) {
		err := rows.Close()
		if err != nil {
			logz.Logger.Error("Unable to close rows connection",
				zap.Error(err))
		}
	}(rows)
	events := GetEvents{}
	for rows.Next() {
		var username string
		var animalName string
		var event string
		var timestamp time.Time
		err := rows.Scan(&username, &animalName, &event, &timestamp)
		if err != nil {
			logz.Logger.Error("Unable to scan results",
				zap.Error(err))
		}
		logz.Logger.Debug("event",
			zap.String("event", event))
		logz.Logger.Debug("timestamp",
			zap.String("timestamp", timestamp.Format("03 04 05 PM - Monday 2006 01 02 ")),
			zap.Time("realtime", timestamp))

		currentEvent := GetEvent{
			Username:   username,
			Event:      event,
			AnimalName: animalName,
			Timestamp:  timestamp.Format("03 04 05 PM - Monday 2006 01 02"),
		}

		events.Events = append(events.Events, currentEvent)
	}
	c.Header("Content-Type", "application/json")
	c.JSON(http.StatusOK, events)
}

func (api *API) submitEvents(c *gin.Context) {
	data := new(Event)
	decoder := json.NewDecoder(c.Request.Body)

	if err := decoder.Decode(data); err != nil {
		logz.Logger.Error("Unable to bind data",
			zap.Error(err))

		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, err)
		return
	}
	tx, err := pg.DB.Begin()
	stmt, err := tx.Prepare(`INSERT INTO events(user_name,animal_name,event,event_timestamp) VALUES($1,$2,$3,$4)`)
	if err != nil {
		logz.Logger.Error("Unable to prepare statement",
			zap.Error(err))
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, err)
		return
	}

	defer func(stmt *sql.Stmt) {
		err := stmt.Close()
		if err != nil {
			logz.Logger.Error("Unable to close statement",
				zap.Error(err))
		}
	}(stmt)
	currentTime := time.Now()
	logz.Logger.Info("Time",
		zap.String("currentTimeformat", currentTime.Format("2006-01-02 15:04:05")),
		zap.Time("currenttime", currentTime))
	stmt.QueryRow(data.Username, data.AnimalName, data.Event, currentTime.Format("2006-01-02 15:04:05"))
	err = tx.Commit()

	if err != nil {
		logz.Logger.Error("Unable to commit query",
			zap.Error(err))
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusBadRequest, err)
		return
	}

	c.Header("Content-Type", "application/json")
	c.JSON(http.StatusAccepted, nil)
	logz.Logger.Info("submitted event",
		zap.String("event", data.Event),
		zap.String("animal", data.AnimalName))
}

func (api *API) InitializeRoutes(apiRouter *gin.RouterGroup) {
	apiRouter.GET("/healthcheck", api.healthcheck)
	apiRouter.GET("/events", api.getEvents)
	apiRouter.POST("/event", api.submitEvents)

}

func (api *API) healthcheck(c *gin.Context) {
	data := health{
		Status: "Up????",
	}
	c.Header("Content-Type", "application/json")
	c.JSON(http.StatusOK, data)
}
