package web

import (
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/navenest/golang-library/pkg/logger/log"
	logz "gitlab.com/navenest/puppy-tracker/pkg/logger/log"
	apiv1 "gitlab.com/navenest/puppy-tracker/web/api/v1"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gin-gonic/gin/otelgin"
	"go.uber.org/zap"
	"net/http"
	"slices"
	"time"
)

type App struct {
	Router    *gin.Engine
	ApiRouter *gin.RouterGroup
	apiv1     *apiv1.API
}

type health struct {
	Status string `json:"status"`
}

func chiLogger() func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			ww := middleware.NewWrapResponseWriter(w, r.ProtoMajor)

			t1 := time.Now()
			defer func() {
				logz.Logger.Info("web response",
					zap.String("client_ip", r.RemoteAddr),
					zap.String("method", r.Proto),
					zap.String("path", r.URL.Path),
					zap.Int("status_code", ww.Status()),
					zap.Int("body_size", ww.BytesWritten()),
					zap.Duration("latency", time.Since(t1)),
				)
			}()

			next.ServeHTTP(ww, r)
		}
		return http.HandlerFunc(fn)
	}
}

func (web *App) loggingMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		ignorePaths := []string{"/healthcheck"}
		if slices.Contains(ignorePaths, c.Request.URL.Path) {
			return
		}
		start := time.Now()
		c.Next()
		latency := time.Since(start)
		log.Logger.Info("web response",
			zap.String("client_ip", c.ClientIP()),
			zap.String("method", c.Request.Method),
			zap.String("path", c.Request.URL.Path),
			zap.Int("status_code", c.Writer.Status()),
			zap.Int("body_size", c.Writer.Size()),
			zap.Duration("latency", latency))
	}
}

func (web *App) Initialize() {
	gin.SetMode(gin.ReleaseMode)
	web.Router = gin.New()
	web.Router.Use(otelgin.Middleware("gin"))
	web.Router.Use(web.loggingMiddleware())
	web.initializeRoutes()
	//p := prometheus.NewPrometheus("echo", nil)
	//p.Use(web.Router)

	//web.Router.Use(middleware.GzipWithConfig(middleware.GzipConfig{
	//	Skipper: func(c echo.Context) bool {
	//		return strings.Contains(c.Path(), "metrics") // Change "metrics" for your own path
	//	},
	//}))
}

func (web *App) Run(addr string) {
	if addr == "" {
		addr = ":8080"
	}
	err := http.ListenAndServe(addr, web.Router)
	if err != nil {
		logz.Logger.Fatal("Issues with webserver: "+err.Error(),
			zap.Error(err))
	}
}

func (web *App) initializeRoutes() {
	web.Router.Use(static.Serve("/", static.LocalFile("dist", false)))
	web.ApiRouter = web.Router.Group("/api/v1")
	web.apiv1.InitializeRoutes(web.ApiRouter)
	web.Router.GET("/healthcheck", web.healthcheck)
}

//func (web *App) healthcheck(c echo.Context) error {
//	data := health{
//		Status: "UP",
//	}
//	return c.JSON(http.StatusOK, data)
//}

func (web *App) healthcheck(c *gin.Context) {
	data := health{
		Status: "UP",
	}
	c.JSON(http.StatusOK, data)
}
